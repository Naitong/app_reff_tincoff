//
//  GeoCell.swift
//  App_Reff_Tincoff
//
//  Created by admin on 15.03.2018.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class GeoCell: UITableViewCell {

    @IBOutlet weak var txtPurchaseAmount: UILabel!
    @IBOutlet weak var txtPlaceName: UILabel!
    @IBOutlet weak var txtCreditCardName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
