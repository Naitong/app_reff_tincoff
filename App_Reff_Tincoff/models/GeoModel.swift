//
//  GeoModel.swift
//  App_Reff_Tincoff
//
//  Created by admin on 15.03.2018.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation

class GeoModel {
    let creditCardName: String
    let placeName: String
    let purchaseAmount: String
    
    init (creditCardName: String, placeName: String, purchaseAmount: String){
        self.creditCardName = creditCardName
        self.placeName = placeName
        self.purchaseAmount = purchaseAmount
    }
}

