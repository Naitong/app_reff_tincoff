//
//  SecondViewController.swift
//  App_Reff_Tincoff
//
//  Created by admin on 15.03.2018.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    fileprivate var dataToDisplay = [GeoModel] ()
    fileprivate let cellIdentifier = String(describing: GeoCell.self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "GeoNib", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        // Do any additional setup after loading the view.
        dataToDisplay.append(GeoModel(creditCardName: "TinkoffBlack", placeName: "KFC", purchaseAmount: "146р"))
        dataToDisplay.append(GeoModel(creditCardName: "TinkoffDeb", placeName: "ASHAN", purchaseAmount: "8560р"))
       dataToDisplay.append(GeoModel(creditCardName: "TinkoffWhite", placeName: "RGD", purchaseAmount: "1250р"))
         tableView.reloadData()
        tableView.tableFooterView = UIView(frame: CGRect.zero)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SecondViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataToDisplay.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: GeoCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! GeoCell
        cell.txtCreditCardName.text = dataToDisplay[indexPath.row].creditCardName
        cell.txtPlaceName.text = dataToDisplay[indexPath.row].placeName
        cell.txtPurchaseAmount.text = dataToDisplay[indexPath.row].purchaseAmount
        return cell
      
    }
}
